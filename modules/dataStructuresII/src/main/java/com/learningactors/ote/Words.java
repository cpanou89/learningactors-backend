package com.learningactors.ote;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.learningactors.ote.Utils.*;

public class Words {
    public static void main(String[] args) {
        //Φέρε το κείμενο ως ένα Array απο String
        String[] words = getWords();
        //Τύπωσε ποιο είναι το μήκος της κάθε λέξης του κειμένου.
        countWordsLength(words);
        //Τύπωσε πόσες φορές εμφανίζεται η κάθε λέξη στο κείμενο.
        countWordsOccurrences(words);
        //Τύπωσε τη μεγαλυτερη σε μήκος λέξη και το μήκος της.
        findLongestWord(words);
        //Τύπωσε τη λέξη που εμφανίζεται περισσότερες φορές στο κείμενο και πόσες είναι.
        findMostOccurredWord(words);
        //Τύπωσε τις διακριτές λέξεις που εμφανίζονται στο κείμενο
        // καθώς και τις διπλότυπες με τη σειρά εμφάνισης τους
        countDistinctAndPrintNonDistinctWords(words);
        //Τύπωσε τις διακριτές λέξεις που εμφανίζονται στο κείμενο με λεξικογραφική (φυσική) σειρά
        printDistinctWordsInLexicographicOrder(words);
        //Τύπωσε τις διακριτές λέξεις με τη σειρά που εμφανίζονται στο κείμενο
        // αφου αφαιρεσεις (φιλτραρεις) οσες εμφανίζονται ακριβώς 3 φορές
        removeWordsThatOccurExactlyThreeTimes(words);
        //Τύπωσε με λεξικογραφική (φυσική) σειρά τις διακριτές λέξεις που εμφανίζονται στο κείμενο
        // αφου αφαιρεσεις (φιλτραρεις) οσες εχουν μήκος μεγαλύτερο απο 3 γράμματα
        removeWordsLongerThanThreeLetters(words);
    }

    private static String[] getWords() {
        String jfk = "We choose to go to the moon. " +
                "We choose to go to the moon in this decade and do the other things, " +
                "not because they are easy, " +
                "but because they are hard, " +
                "because that goal will serve to organize and measure the best of our energies and skills, " +
                "because that challenge is one that we are willing to accept, " +
                "one we are unwilling to postpone, " +
                "and one which we intend to win, " +
                "and the others, too.";
        return jfk
                .toLowerCase()
                .replace(".", "")
                .replace(",", "")
                .split(" ");
    }

    private static void countWordsLength(String[] words) {
        String wordsLength = Arrays.stream(words)
                .map(word -> word + ":" + word.length())
                .collect(Collectors.joining(", "));
        System.out.println("countWordsLength:\n\t" + wordsLength);
    }

    private static void countWordsOccurrences(String[] words) {
        System.out.println("countWordsOccurrences:\n\t" + mapWordsWithOccurrencesDistinct(words, false));
    }

    private static void findLongestWord(String[] words) {
        Map.Entry<String, Integer> longest = mapWordsWithLength(words)
                .entrySet().stream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .orElse(null);
        System.out.println("findLongestWord:\n\t" + (longest != null ? longest : "Could not find Longest Word"));
    }

    private static void findMostOccurredWord(String[] words) {
        Map.Entry<String, Integer> mostOccurred = mapWordsWithOccurrencesDistinct(words, false)
                .entrySet().stream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .orElse(null);
        System.out.println("findMostOccurredWord:\n\t" + (mostOccurred != null ? mostOccurred : "Could not find Most Occurred Word"));
    }

    private static void countDistinctAndPrintNonDistinctWords(String[] words) {
        Map<String, Integer> wordOccurrenceMap = mapWordsWithOccurrencesDistinct(words, true);
        Map<Boolean, List<Map.Entry<String, Integer>>> partitioned = partitionMapToDistinctNotDistinct(wordOccurrenceMap);
        System.out.println("countDistinctWords in encounter order:\n\t" + partitioned.get(true));
        System.out.println("PrintNonDistinctWords in encounter order:\n\t" + partitioned.get(false));
    }

    private static void printDistinctWordsInLexicographicOrder(String[] words) {
        Map<String, Integer> wordOccurrenceMap = mapWordsWithOccurrencesDistinct(words, false);
        Map<Boolean, List<Map.Entry<String, Integer>>> partitionedMap = partitionMapToDistinctNotDistinct(wordOccurrenceMap);
        String lexicographicWords = partitionedMap.get(true).stream()
                .map(Map.Entry::getKey)
                .sorted()
                .collect(Collectors.joining(", "));
        System.out.println("printDistinctWords in lexicographic order:\n\t" + lexicographicWords);
    }

    private static void removeWordsThatOccurExactlyThreeTimes(String[] words) {
        Map<String, Integer> map = mapWordsWithOccurrencesDistinct(words, true);
        map.entrySet().removeIf(word -> word.getValue() == 3);
        System.out.println("removeWordsThatOccurExactlyThreeTimes in encounter order:\n\t" + map);
    }

    private static void removeWordsLongerThanThreeLetters(String[] words) {
        List<Map.Entry<String, Integer>> sortedWordsLessThan3 = mapWordsWithLength(words)
                .entrySet().stream()
                .filter(entry -> entry.getValue() <= 3)
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toList());
        System.out.println("removeWordsLongerThanThreeLetters in lexicographic order:\n\t" + sortedWordsLessThan3);
    }

}