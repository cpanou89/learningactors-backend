package com.learningactors.ote;

import java.util.*;
import java.util.stream.Collectors;

public class Utils {

    /**
     * Accepts an Array of Strings based on which the method will generate a Map with words(String) as keys and Length(Integer) as Values
     *
     * @param words array of Strings.
     * @return a new new HashMap<String,Integer>.
     */
    public static Map<String, Integer> mapWordsWithLength(String[] words) {
        return Arrays.stream(words)
                .distinct()
                .collect(Collectors.toMap(word -> word, String::length));
    }

    /**
     * Accepts an Array of Strings based on which the method will generate a Map with the Strings (words) as keys and number of occurrences as Values
     * @param words array of Strings
     * @param ordering boolean to indicate ordering.
     *                 If true the resulting map is a LinkedHashMap with its entrySet ordered in insertion (encounter) order.
     *                 If false the resulting map will be a plain HashMap.
     * @return a new LinkedHashMap if Encounter order matters, or a new HashMap if not.
     */
    public static Map<String, Integer> mapWordsWithOccurrencesDistinct(String[] words, boolean ordering) {
        Map<String, Integer> map = ordering ? new LinkedHashMap<>() : new HashMap<>();
        Arrays.stream(words).forEachOrdered(word -> {
            map.computeIfPresent(word, (foundWord, occurrences) -> ++occurrences);
            map.putIfAbsent(word, 1);
        });
        return map;
    }

    /**
     * Partitions a Map<String,Integer> in two Lists<Map.Entry>.
     *
     * @param wordOccurrenceMap Map that contains words as keys and occurrences as value.
     * @return a new Map with Boolean keys and a List of Map.Entries for value.
     *          map.get(true) returns the list of distinct word entries
     *          map.get(false) returns the list of non-distinct words.
     */
    public static Map<Boolean, List<Map.Entry<String, Integer>>> partitionMapToDistinctNotDistinct(Map<String, Integer> wordOccurrenceMap) {
        return wordOccurrenceMap
                .entrySet().stream()
                .collect(Collectors.partitioningBy(entry -> entry.getValue() == 1));
    }

}
