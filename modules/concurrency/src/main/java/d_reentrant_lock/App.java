package d_reentrant_lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class App {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = new Counter();

        Thread increase10_thousands = new Thread(() -> {
            for(int i=0; i < 10_000; i++) {
                counter.add(1);
            }
        });

        Thread decrease10_thousands = new Thread(() -> {
            for(int i=0; i < 10_000; i++) {
                counter.remove(1);
            }
        });

        increase10_thousands.start();
        decrease10_thousands.start();

        increase10_thousands.join();
        decrease10_thousands.join();


        System.out.println(counter.count);
    }

}

class Counter {

    Lock lock = new ReentrantLock();

    protected long count = 0;

    public void add(long value) {

        /** 1st usage */
        lock.lock();
        try {
            this.count = this.count + value;
        } finally {
            lock.unlock();
        }

        /** 2nd usage */
        if(lock.tryLock()) {
            try {
                this.count = this.count + value;
            } finally {
                lock.unlock();
            }
        } else {
            System.out.println("cannot acquire lock right now");
        }

    }

    public void remove(long value) {
        lock.lock();
        try {
            this.count = this.count - value;
        } finally {
            lock.unlock();
        }

    }
}