package f_futures;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class App {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Future<Integer> result = new SquareCalculator().calculate(2);
        System.out.println("I am going to ask for the value....");
        System.out.println(result.get());
        System.out.println("I received the value....");
    }
}

class SquareCalculator {

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public Future<Integer> calculate(Integer input) {
        return executor.submit(() -> {
            Thread.sleep(5000);
            return input * input;
        });
    }
}
