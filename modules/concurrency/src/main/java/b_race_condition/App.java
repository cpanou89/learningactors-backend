package b_race_condition;

public class App {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = new Counter();

        Thread increase10_thousands = new Thread(() -> {
            for (int i = 0; i < 10_000; i++) {
                counter.add(1);
            }
        });

        Thread decrease10_thousands = new Thread(() -> {
            for (int i = 0; i < 10_000; i++) {
                counter.remove(1);
            }
        });

        increase10_thousands.start();
        decrease10_thousands.start();

        increase10_thousands.join();
        decrease10_thousands.join();


        System.out.println(counter.count);
    }

}

//TODO synchronize methods
class Counter {

    protected long count = 0;

    public void add(long value) {
        synchronized (this) {
            this.count = this.count + value;
        }
    }

    public synchronized void remove(long value) {
        this.count = this.count - value;
    }
}