package e_thread_communication.backpressure;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static e_thread_communication.backpressure.Utils.INPUT_FILE;
import static e_thread_communication.backpressure.Utils.OUTPUT_FILE;


public class MainApplication {

    public static void main(String[] args) throws IOException {
        ThreadSafeQueue threadSafeQueue = new ThreadSafeQueue();
        File inputFile = new File(INPUT_FILE);
        File outputFile = new File(OUTPUT_FILE);

        MatricesReaderProducer matricesReader = new MatricesReaderProducer(new FileReader(inputFile), threadSafeQueue);
        MatricesMultiplierConsumer matricesConsumer = new MatricesMultiplierConsumer(new FileWriter(outputFile), threadSafeQueue);

        matricesConsumer.start();
        matricesReader.start();
    }

}