package e_thread_communication.backpressure;

import java.io.FileReader;
import java.util.Scanner;

import static e_thread_communication.backpressure.Utils.N;

public class MatricesReaderProducer extends Thread {
    private final Scanner scanner;
    private final ThreadSafeQueue queue;

    public MatricesReaderProducer(FileReader reader, ThreadSafeQueue queue) {
        this.scanner = new Scanner(reader);
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            int[][] matrix1 = readMatrix();
            int[][] matrix2 = readMatrix();
            if (matrix1 == null || matrix2 == null) {
                queue.terminate();
                System.out.println("No more matrices to read. Producer Thread is terminating");
                return;
            }

            MatricesPair matricesPair = new MatricesPair();
            matricesPair.matrix1 = matrix1;
            matricesPair.matrix2 = matrix2;

            queue.add(matricesPair);
        }
    }

    private int[][] readMatrix() {
        int[][] matrix = new int[N][N];
        for (int r = 0; r < N; r++) {
            if (!scanner.hasNext()) {
                return null;
            }
            String[] line = scanner.nextLine().split("\\|");
            for (int c = 0; c < N; c++) {
                matrix[r][c] = Integer.parseInt(line[c].trim());
            }
        }
        scanner.nextLine();
        return matrix;
    }
}

