package e_thread_communication.backpressure;

import java.util.LinkedList;
import java.util.Queue;


//TODO Fix the add and remove in order to achieve backpressure with the value of the CAPACITY
public class ThreadSafeQueue {
    private final Queue<MatricesPair> queue = new LinkedList<>();
    private boolean isEmpty = true;
    private boolean isTerminate = false;
    private final int CAPACITY = 5;

    public synchronized void add(MatricesPair matricesPair) {


        queue.add(matricesPair);
        isEmpty = false;
        notify();
    }

    public synchronized MatricesPair remove() {
        while (isEmpty && !isTerminate) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if(queue.size() == 1) {
            isEmpty = true;
        }

        if(queue.size() == 0) {
            return null;
        }

        System.out.println("Queue size: "+ queue.size());
        return queue.remove();

    }

    public synchronized void terminate() {
        isTerminate = true;
        notifyAll();
    }
}