package e_thread_communication.backpressure;

import java.io.FileWriter;
import java.io.IOException;

import static e_thread_communication.backpressure.Utils.multiplyMatrices;
import static e_thread_communication.backpressure.Utils.saveMatrixToFile;

public class MatricesMultiplierConsumer extends Thread {
    private final ThreadSafeQueue queue;
    private final FileWriter fileWriter;

    public MatricesMultiplierConsumer(FileWriter fileWriter, ThreadSafeQueue queue) {
        this.fileWriter = fileWriter;
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            MatricesPair matricesPair = queue.remove();
            if (matricesPair == null) {
                System.out.println("No more matrices to read from the queue, consumer is terminating");
                break;
            }

            int[][] result = multiplyMatrices(matricesPair.matrix1, matricesPair.matrix2);

            try {
                saveMatrixToFile(fileWriter, result);
            } catch (IOException ignored) { }
        }

        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
