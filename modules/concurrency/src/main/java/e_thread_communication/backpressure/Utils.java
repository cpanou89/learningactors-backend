package e_thread_communication.backpressure;

import java.io.FileWriter;
import java.io.IOException;
import java.util.StringJoiner;

public class Utils {

    public static final String INPUT_FILE = "./out/matrices";
    public static final String OUTPUT_FILE = "./out/matrices_results.txt";
    public static final int N = 10;

    public static int[][] multiplyMatrices(int[][] m1, int[][] m2) {

        int[][] result = new int[N][N];
        for (int r = 0; r < N; r++) {
            for (int c = 0; c < N; c++) {
                for (int k = 0; k < N; k++) {
                    result[r][c] += m1[r][k] * m2[k][c];
                }
            }
        }
        return result;
    }

    public static void saveMatrixToFile(FileWriter fileWriter, int[][] matrix) throws IOException {
        for (int r = 0; r < N; r++) {
            StringJoiner stringJoiner = new StringJoiner(", ");
            for (int c = 0; c < N; c++) {
                stringJoiner.add(String.format("%d", matrix[r][c]));
            }
            fileWriter.write(stringJoiner.toString());
            fileWriter.write('\n');
        }
        fileWriter.write('\n');
    }
}
