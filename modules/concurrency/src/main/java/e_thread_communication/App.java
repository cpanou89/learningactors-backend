package e_thread_communication;

import java.math.BigInteger;

public class App {

    public static void main(String[] args) {
        Thread thread = new Thread(new BlockingTask());
        thread.start();
        thread.interrupt();

        Thread longRunningTask = new Thread(new LongRunningTask());
        longRunningTask.start();
        longRunningTask.interrupt();


        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    static class BlockingTask implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(300000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class LongRunningTask implements Runnable {

        @Override
        public void run() {
           BigInteger base = new BigInteger("200000");
           BigInteger power = new BigInteger("200000");
           BigInteger result = BigInteger.ONE;
           for(BigInteger i = BigInteger.ZERO; i.compareTo(power) != 0; i =i.add(BigInteger.ONE)) {
               if(Thread.currentThread().isInterrupted()) {
                   System.out.println("Interrupted");
                   throw new RuntimeException("Thread exited");
               }
               result = result.multiply(base);
           }
            System.out.println(result);
        }
    }
}
