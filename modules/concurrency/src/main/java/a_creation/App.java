package a_creation;

public class App {


    public static void main(String[] args) {
        System.out.println("*****************  Starting Program *******************");
        createThreadWithRunnable();
        createThreadByExtendingThread();
        System.out.println("*****************  End Program  ***********************");

    }

    public static void createThreadWithRunnable() {
        Runnable task = () -> {
            try {
                System.out.println("Hello from the runnable task");
                Thread.sleep(200);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }

    public static void createThreadByExtendingThread() {

        class MyThread extends Thread {
            @Override
            public void run() {
                try {
                    System.out.println("Hello from the Thread");
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        Thread thread = new MyThread();
        thread.start();
    }
}
