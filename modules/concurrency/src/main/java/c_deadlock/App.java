package c_deadlock;

public class App {

    public static void main(String[] args) {
        Printer printer = new Printer();
        MailServer server = new MailServer();

        Thread th = new PrintAndSendMail(printer, server);
        Thread th2 = new SendMailAndPrint(printer, server);

        th.start();
        th2.start();
    }
}

class PrintAndSendMail extends Thread {
    private final Printer printer;
    private final MailServer mailServer;

    PrintAndSendMail(Printer printer, MailServer mailServer) {
        this.printer = printer;
        this.mailServer = mailServer;
    }

    @Override
    public void run() {
        synchronized (printer) {
            printer.print("message1");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (mailServer) {
               mailServer.sendMail("asdasdsa");
            }
        }
    }

}

class SendMailAndPrint extends Thread {
    private final Printer printer;
    private final MailServer mailServer;

    SendMailAndPrint(Printer printer, MailServer mailServer) {
        this.printer = printer;
        this.mailServer = mailServer;
    }

    @Override
    public void run() {
        synchronized (mailServer) {
            mailServer.sendMail("mail1");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (printer) {
                printer.print("message1");
            }
        }
    }
}


class Printer {

    public void print(String message){
        System.out.println("Printer is printing " + message);
    }
}

class MailServer {

    public void sendMail(String mail){

        System.out.println("Mail server is sending " + mail);

    }
}