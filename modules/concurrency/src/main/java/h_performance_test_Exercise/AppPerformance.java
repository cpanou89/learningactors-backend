package h_performance_test_Exercise;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static h_performance_test_Exercise.Utils.TIME_TO_SLEEP;
import static h_performance_test_Exercise.Utils.initializeList;

public class AppPerformance {


    public static void main(String[] args) throws InterruptedException, ExecutionException {

        List<Long> bigList = new ArrayList<>();
        initializeList(bigList);
        calculateSumInSingleThread(bigList);
        calculateSumInMultipleThread(bigList, 3);
        AppPerformanceExecutors.calculateSumInExecutorThread(bigList, 3);
    }

    private static void calculateSumInSingleThread(List<Long> list) {
        long start = System.currentTimeMillis();
        long sum = 0;
        try {
            sum = calculateSum(list);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();

        System.out.println(sum + " in milliseconds: " + (end - start));
    }


    private static void calculateSumInMultipleThread(List<Long> list, int numOfThreads) throws InterruptedException {
        long start = System.currentTimeMillis();

        int sizeOfList = (int) Math.ceil((float) list.size() / numOfThreads);
        final List<List<Long>> chunks = Lists.partition(list, sizeOfList);
        long[] results = new long[chunks.size()];
        Thread[] threads = new Thread[chunks.size()];

        for (int index = 0; index < chunks.size(); index++) {
            Thread t = new Thread(new SumCalculator(chunks.get(index), index, results));
            threads[index] = t;
            t.start();
        }

        for (int index = 0; index < chunks.size(); index++) {
            threads[index].join();
        }

        long sum = 0L;
        for (long result : results) {
            sum = sum + result;
        }

        long end = System.currentTimeMillis();

        System.out.println(sum + " in milliseconds: " + (end - start));
    }

    private static Long calculateSum(List<Long> list) throws InterruptedException {
        long sum = 0L;
        for (Long value : list) {
            Thread.sleep(TIME_TO_SLEEP);
            sum = sum + value;
        }
        return sum;
    }

    static class SumCalculator implements Runnable {

        private final List<Long> list;
        private final int index;
        private final long[] results;

        SumCalculator(List<Long> list, int index, long[] results) {
            this.list = list;
            this.index = index;
            this.results = results;
        }

        private void sumChunk(List<Long> list, int index, long[] results) throws InterruptedException {
            long sum = 0;
            for (Long value : list) {
                Thread.sleep(TIME_TO_SLEEP);
                sum = sum + value;
            }
            results[index] = sum;
        }


        @Override
        public void run() {
            if (results != null) {
                try {
                    sumChunk(this.list, index, results);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
