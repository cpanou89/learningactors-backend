package h_performance_test_Exercise;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static h_performance_test_Exercise.Utils.TIME_TO_SLEEP;
import static h_performance_test_Exercise.Utils.initializeList;

public class AppPerformanceExecutors {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        List<Long> bigList = new ArrayList<>();
        initializeList(bigList);
        calculateSumInExecutorThread(bigList, 3);
    }


    public static void calculateSumInExecutorThread(List<Long> list, int numOfThreads) throws InterruptedException, ExecutionException {

        int sizeOfList = (int) Math.ceil((float) list.size() / numOfThreads);
        final List<List<Long>> chunks = Lists.partition(list,sizeOfList);

        long start = System.currentTimeMillis();
        long result = 0;

        //TODO Implement the logic with executors
        long end = System.currentTimeMillis();

        System.out.println(result + " in milliseconds: "+ (end - start));
    }

    private static class ListSumCalculator implements Callable<Long> {

        private final List<Long> list;

        private ListSumCalculator(List<Long> list) {
            this.list = list;
        }


        @Override
        public Long call() throws Exception {
            return  list.stream().peek(num -> {
                try {
                    Thread.sleep(TIME_TO_SLEEP);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).reduce(0L, Long::sum);
        }
    }
}
