package com.company.shapes.model;

import com.company.shapes.base.Shape3D;

public class Cube extends Shape3D {
    public Cube(double area) {
        super(area);
    }
}
