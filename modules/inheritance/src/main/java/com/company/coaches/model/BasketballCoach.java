package com.company.coaches.model;

import com.company.coaches.base.Coach;

public class BasketballCoach implements Coach {

    protected BasketballCoach() {
    }

    @Override
    public void warmup() {
        System.out.println("warmup: Run circles");
    }

    @Override
    public void training() {
        System.out.println("training: Layups and free-throws");
    }

    @Override
    public void recovery() {
        System.out.println("recovery: Run circles");
    }
}
