package com.company.coaches.model;

import com.company.coaches.base.Coach;

public class FootballCoach implements Coach {
    protected FootballCoach() {
    }

    @Override
    public void warmup() {
        System.out.println("warmup: Jumping Jacks");
    }

    @Override
    public void training() {
        System.out.println("training: Passing");
    }

    @Override
    public void recovery() {
        System.out.println("recovery: Spa");
    }
}
