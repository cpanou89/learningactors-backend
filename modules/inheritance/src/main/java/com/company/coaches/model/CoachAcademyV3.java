package com.company.coaches.model;

import com.company.coaches.base.AcademyV2;
import com.company.coaches.base.Coach;
import com.company.coaches.base.Sport;

public class CoachAcademyV3 implements AcademyV2 {

    private CoachAcademyV3() {
    }

    public static Coach fetchCoach(Sport sport) {
        return new CoachAcademyV3().getCoach(sport);
    }

    public Coach getCoach(Sport sport) {
        switch (sport) {
            case TENNIS:
                return new TennisCoach();
            case FOOTBALL:
                return new FootballCoach();
            case BASKETBALL:
                return new BasketballCoach();
            case CROSSFIT:
                return new CrossfitCoach();
            default:
                return new Coach() {
                };
        }
    }
}
