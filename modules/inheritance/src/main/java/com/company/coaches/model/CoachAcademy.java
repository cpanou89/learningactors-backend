package com.company.coaches.model;

import com.company.coaches.base.Academy;
import com.company.coaches.base.Coach;
import com.company.coaches.base.Sport;

public class CoachAcademy implements Academy {

    private final Sport sport;

    private CoachAcademy(Sport sport) {
        this.sport = sport;
    }

    public static Academy of(Sport sport) {
        return new CoachAcademy(sport);
    }

    public Coach getCoach() {
        switch (sport) {
            case TENNIS:
                return new TennisCoach();
            case FOOTBALL:
                return new FootballCoach();
            case BASKETBALL:
                return new BasketballCoach();
            case CROSSFIT:
                return new CrossfitCoach();
            default:
                return new Coach() {
                };
        }
    }
}
