package com.company.coaches.model;

import com.company.coaches.base.AcademyV2;
import com.company.coaches.base.Coach;
import com.company.coaches.base.Sport;

public class CoachAcademyV2 implements AcademyV2 {

    public static final CoachAcademyV2 instance = new CoachAcademyV2();

    private CoachAcademyV2() {
    }

    public static AcademyV2 getInstance() {
        return instance;
    }

    public Coach getCoach(Sport sport) {
        switch (sport) {
            case TENNIS:
                return new TennisCoach();
            case FOOTBALL:
                return new FootballCoach();
            case BASKETBALL:
                return new BasketballCoach();
            case CROSSFIT:
                return new CrossfitCoach();
            default:
                return new Coach() {
                };
        }
    }
}
