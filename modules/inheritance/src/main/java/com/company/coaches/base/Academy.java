package com.company.coaches.base;

public interface Academy {

    default Coach getCoach() {
        return new Coach() {
        };
    }

}
