package com.company.coaches.base;

public enum Sport {
    BASKETBALL, FOOTBALL, TENNIS, CROSSFIT, DEFAULT
}
