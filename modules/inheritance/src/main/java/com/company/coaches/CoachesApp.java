package com.company.coaches;

import com.company.coaches.base.AcademyV2;
import com.company.coaches.base.Coach;
import com.company.coaches.base.Sport;
import com.company.coaches.model.CoachAcademy;
import com.company.coaches.model.CoachAcademyV2;
import com.company.coaches.model.CoachAcademyV3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CoachesApp {

    public static void run() {
        // write your code here
        List<Coach> coaches = new ArrayList<>();

        Stream.of(Sport.values()).forEach(sport -> coaches.add(CoachAcademy.of(sport).getCoach()));

        AcademyV2 academyV2 = CoachAcademyV2.getInstance();
        Stream.of(Sport.values()).forEach(sport -> coaches.add(academyV2.getCoach(sport)));

        List<Coach> more = Stream.of(Sport.values()).map(CoachAcademyV3::fetchCoach).collect(Collectors.toList());
        coaches.addAll(more);

        coaches.forEach(coach -> {
            System.out.println("-------------" + coach.getClass().getSimpleName() + "-------------");
            coach.warmup();
            coach.training();
            coach.recovery();
        });
    }

}
