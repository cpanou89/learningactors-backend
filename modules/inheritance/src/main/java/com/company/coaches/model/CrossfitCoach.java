package com.company.coaches.model;

import com.company.coaches.base.Coach;

public class CrossfitCoach implements Coach {
    protected CrossfitCoach() {
    }

    @Override
    public void warmup() {
        System.out.println("warmup: Dead-lift");
    }

    @Override
    public void training() {
        System.out.println("training: Dead-lifts");
    }

    @Override
    public void recovery() {
        System.out.println("recovery: more Dead-lifts");
    }
}
