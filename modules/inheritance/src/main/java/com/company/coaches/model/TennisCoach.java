package com.company.coaches.model;

import com.company.coaches.base.Coach;

public class TennisCoach implements Coach {
    protected TennisCoach() {
    }

    @Override
    public void warmup() {
        System.out.println("warmup: throw the ball at the ball boy");
    }

    @Override
    public void training() {
        System.out.println("training: serve");
    }

    @Override
    public void recovery() {
        System.out.println("recovery: Cookies and Tea");
    }
}
