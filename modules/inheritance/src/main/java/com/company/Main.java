package com.company;

import com.company.coaches.CoachesApp;
import com.company.shapes.ShapesApp;

public class Main {
    public static void main(String[] args) {
        System.out.println("\n\n");
        CoachesApp.run();
        ShapesApp.run();
        System.out.println("\n\n");
    }
}
