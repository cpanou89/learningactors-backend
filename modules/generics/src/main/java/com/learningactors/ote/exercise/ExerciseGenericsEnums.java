package com.learningactors.ote.exercise;

import java.util.ArrayList;
import java.util.List;

/**
 * 1. Fix the FavoriteClasses class to use generics for
 * * its three variables favorite1,favorite2,and favorite3,
 * * its constructor,
 * * its three functions to return each variable
 * * as well as fixing r in the main section.
 * <p>
 * 2. Then define a variable of the FavoriteClasses class and use "Hello", 67, 6.3 as the arguments for the constructor,
 * and when you define it use your favorite classes/types that go with the three arguments.
 * <p>
 * 3. Can we use the Priority enum as a generic? If so please show an example
 * <p>
 * 4. Please declare an Enum with custom constructor and use it
 */
public class ExerciseGenericsEnums {

    private enum Priority {
        HIGH, MEDIUM, LOW
    }


    /**
     * 4.
     */
    private enum Month {
        JANUARY(1, "JANUARY"),
        FEBRUARY(2, "FEBRUARY"),
        MARCH(3, "MARCH"),
        APRIL(4, "APRIL"),
        MAY(5, "MAY"),
        JUNE(6, "JUNE"),
        JULY(7, "JULY"),
        AUGUST(8, "AUGUST"),
        SEPTEMBER(9, "SEPTEMBER"),
        OCTOBER(10, "OCTOBER"),
        NOVEMBER(11, "NOVEMBER"),
        DECEMBER(12, "DECEMBER"),
        ;
        private final int num;
        private String name;

        Month(int num, String name) {
            this.num = num;
            this.name = name;
        }
    }

    /**
     * 1
     */
    private static class FavoriteClasses<F1, F2, F3> {
        private final F1 favorite1;
        private final F2 favorite2;
        private final F3 favorite3;

        FavoriteClasses(F1 fav1, F2 fav2, F3 fav3) {
            this.favorite1 = fav1;
            this.favorite2 = fav2;
            this.favorite3 = fav3;
        }

        public F1 getFav1() {
            return (this.favorite1);
        }

        public F2 getFav2() {
            return (this.favorite2);
        }

        public F3 getFav3() {
            return (this.favorite3);
        }
    }

    public static void main(String[] args) {
        List<Double> r = new ArrayList<>();
        r.add(6.3);
        r.add(5.9);

        /*
         * 2
         */
        FavoriteClasses<String, Integer, Double> a = new FavoriteClasses<>("Hello", 67, r.get(0));
        System.out.println("My favorites are " + a.getFav1() + ", " + a.getFav2() + ", and " + a.getFav3() + ".");

        /*
         * 3 We can use it as a Type parameter.
         */
        FavoriteClasses<Month, Priority, Double> b = new FavoriteClasses<>(Month.APRIL, Priority.HIGH, r.get(0));
        System.out.println("My favorites are " + b.getFav1() + ":" + b.getFav1().num + ", " + b.getFav2().name() + ", and " + b.getFav3() + ".");

        /*
         * 4
         */
        FavoriteClasses<Month, Priority, Double> c = new FavoriteClasses<>(Month.APRIL, Priority.HIGH, r.get(0));
        System.out.println("My favorites are " + c.getFav1() + ":" + c.getFav1().name + ":" + c.getFav1().num + ", " + c.getFav2().name() + ", and " + c.getFav3() + ".");
    }
}