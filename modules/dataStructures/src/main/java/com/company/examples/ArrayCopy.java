package com.company.examples;

import java.util.Arrays;

public class ArrayCopy {

    public static void main(String[] args) {
        char[] copyFrom = {'a', 'b', 'c', 'd', 'e', 'f', 'i', 'a', 'n', 'c', 'e'};
        char[] copyTo = new char[10];

        System.arraycopy(copyFrom, 3, copyTo, 2, copyFrom.length - 3);
        System.out.println(new String(copyTo));

        int[] array = {1, 1, 1, 1, 1};
        int[] anotherArray;

        anotherArray = array;

        System.out.println(Arrays.toString(anotherArray));
        System.out.println(Arrays.toString(array));

        anotherArray[4] = 150;

        System.out.println(Arrays.toString(anotherArray));
        System.out.println(Arrays.toString(array));

    }

}
