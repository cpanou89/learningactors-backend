package com.company.examples;

public class Passenger extends Human {
    private String name;

    public Passenger(String name, int age) {
        super(age);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String greet() {
        return "Passenger{" +
                "name='" + name + '\'' +
                "} " + super.greet();
    }

    @Override
    public String toString() {
        return greet();
    }
}
