package com.company.examples;

import java.util.Arrays;

public class Covariance {

    public static void main(String[] args) {
        Number[] numbers = new Number[3];
        numbers[0] = 1;
        numbers[1] = 1.0f;
        numbers[2] = (byte) 1.5;
        System.out.println(Arrays.toString(numbers));
    }

}
