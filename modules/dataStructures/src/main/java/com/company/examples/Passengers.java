package com.company.examples;

import java.util.Arrays;

public class Passengers {

    private static Passenger[] passengers = new Passenger[1];
    private static int index = 0;

    public static void main(String[] args) {

        add(new Passenger("Kwstas", 25));
        add(new Passenger("Sotiris", 30));
        add(new Passenger("Manwlis", 35));
        System.out.println(Arrays.toString(passengers));

        Bus<Passenger> bus = new Bus<>();
        bus.add(new Passenger("Kwstas", 25));
        bus.add(new Passenger("Sotiris", 30));
        bus.add(new Passenger("Manwlis", 35));
        System.out.println(bus.toString());

    }

    private static void add(Passenger passenger) {
        if (index >= passengers.length - 1)
            passengers = Arrays.copyOf(passengers, passengers.length + 1);
        passengers[index++] = passenger;
    }

}
