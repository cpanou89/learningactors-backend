package com.company.examples;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = new int[10];
        array[0] = 1221;
        array[1] = 1222;
        array[2] = 1223;
        array[3] = 1224;
        array[4] = 1225;
        array[5] = 1226;
        array[6] = 1227;
        array[7] = 1228;
        array[8] = 1229;
        array[9] = 1220;
        System.out.println(Arrays.toString(array));

        int[] myArray = {10, 20, 30, 40, 50, 60};
        System.out.println(Arrays.toString(myArray));

    }

}
