package com.company.exercises;

import java.util.Arrays;


public class MinimumExercise {

    private static Integer[] array = new Integer[]{};

    public static void main(String[] args) {
        boolean finished = false;
        for (int i = 0; !finished; i++) {
            Integer input = BufferedReaderHelper.getInput("Enter number");
            if (input == null)
                finished = true;
            else
                add(i, input);
        }

        System.out.println("min = " + findMinimum(array));

    }

    private static void add(int index, Integer input) {
        if (index >= array.length)
            array = Arrays.copyOf(array, array.length + 1);
        array[index] = input;
    }

    private static Integer findMinimum(Integer[] arrays) {
        return Arrays.stream(arrays).mapToInt(elem -> elem).min().orElseGet(() -> {
            System.out.println("No Min Found returning 0");
            return 0;
        });
    }


}
