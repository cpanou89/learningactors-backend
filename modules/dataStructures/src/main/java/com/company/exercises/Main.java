package com.company.exercises;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        // 1.
        String[] arrayOfTen = getArrayOfTen();
        System.out.println("1. Array with 10 elements: ");
        System.out.println(" " + Arrays.toString(arrayOfTen));

        // 2.
        List<String> linkedList = getListOfTen(LinkedList.class);
        System.out.println("2. LinkedList with 10 elements: ");
        System.out.println(" " + linkedList.toString());

        // 3.
        List<String> arrayList = getListOfTen(ArrayList.class);
        System.out.println("3. ArrayList with 10 elements: ");
        System.out.println(" " + arrayList.toString());

        // 4. Create the most appropriate list collection that contains 10 elements
        //    and remove and 3-rd and 7-th element of the initial collection
        //
        //  LinkedList has a hard time finding the object O(N) but is fast at removing and updating the indexes O(1)
        //  ArrayList is fast at finding the object O(1) but slow at removing because of shifting all the remaining indexes O(N)
        //  linked list wins because of arraylist shifting
        //
        //  In either case to safely remove from the "initial" list we must use an Iterator. Otherwise removing the 3rd element
        //  shifts the remaining items so the indexes will point at different objects after each remove() call

        System.out.println("4. Element Removal:");

        List<String> editableList = getListOfTen(LinkedList.class);
        editableList.remove(2);
        editableList.remove(6);
        System.out.println(" List: " + editableList.toString());

        List<String> safelyEditableList = getListOfTen(ArrayList.class);
        Iterator<String> editableIterator = safelyEditableList.iterator();
        int i = 0;
        while (editableIterator.hasNext()) {
            if (i == 3 || i == 7)
                editableIterator.remove();
            editableIterator.next();
            i++;
        }
        System.out.println(" Iterator: " + safelyEditableList.toString());
        // 5.
        System.out.println("5. Traversals:");

        List<String> listTraversal = getListOfTen(ArrayList.class);
        System.out.print(" ForEach: [ ");
        listTraversal.forEach(element -> System.out.print(element + ", "));
        System.out.println("]");

        Iterator<String> traversalIterator = listTraversal.iterator();
        System.out.print(" Iterator: [ ");
        while (traversalIterator.hasNext()) {
            System.out.print(traversalIterator.next() + (traversalIterator.hasNext() ? ", " : ""));
        }
        System.out.println("]");

        System.out.println("6. ArrayList O(1)");

        System.out.println("7. Arraylist | All! use an iterator");

    }

    private static String[] getArrayOfTen() {
        int index = 0;
        String[] array = new String[10];
        while (index < 10) {
            array[index++] = "String " + index;
        }
        return array;
    }

    private static List<String> getListOfTen(Class<?> tClass) {
        List<String> list;
        if (tClass.equals(ArrayList.class)) {
            list = new ArrayList<>();
        } else if (tClass.equals(LinkedList.class)) {
            list = new LinkedList<>();
        } else {
            return Collections.emptyList();
        }

        for (int i = 1; i <= 10; i++) {
            list.add(new String("Dummy " + i));
        }

        return list;
    }

}
