package com.company.exercises;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BufferedReaderHelper {

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static Integer getInput(String message) {
        String value = "";
        String exitMessage = message + " (\"n\" to finish):";
        System.out.println(exitMessage);
        try {
            value = reader.readLine();
            return Integer.parseInt(value);
        } catch (Exception e) {
            if (value.equals("n"))
                return null;
            System.out.println("wrong input...try again");
            return getInput(message);
        }
    }
}
