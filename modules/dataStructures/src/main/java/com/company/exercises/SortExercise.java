package com.company.exercises;

import java.util.Arrays;
import java.util.Comparator;


public class SortExercise {

    public static void main(String[] args) {
        Integer[] arrays = new Integer[getSize()];
        for (int i = 0; i < arrays.length; i++) {
            Integer input = BufferedReaderHelper.getInput("Enter number");
            if (input == null) {
                System.out.println("Closing...");
                System.exit(0);
            } else {
                arrays[i] = input;
            }
        }

        arrays = sort(arrays);

        System.out.println(Arrays.toString(arrays));

    }

    private static Integer getSize() {
        Integer size = BufferedReaderHelper.getInput("Enter collection size");
        if (size == null || size < 0) {
            System.out.println("Closing...");
            System.exit(0);
        }
        return size;
    }

    private static final Comparator<Integer> descendingComparator = (int1, int2) -> {
        if (int1 - int2 > 0)
            return -1;
        else if (int1 - int2 < 0)
            return 1;
        return 0;
    };

    public static Integer[] sort(Integer[] arrays) {
        Integer[] sorted = Arrays.copyOf(arrays, arrays.length);
//        Arrays.sort(sorted, descendingComparator);
        Arrays.sort(sorted, Comparator.reverseOrder());
        return sorted;
    }


}
