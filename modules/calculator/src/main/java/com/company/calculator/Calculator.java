package com.company.calculator;

public class Calculator {

    public int add(int x, int y) {
        return x + y;
    }

    public int subtract(int x, int y) {
        return x - y;
    }

    public int multiply(int x, int y) {
        return x * y;
    }

    public int divide(int x, int y) {
        return x / y;
    }

    public int pow(int x, int y) {
        return (int) powerOf(x, x, y);
    }

    public double powerOf(int base, int x, int y) {
        if (y == 1)
            return x;
        x = base * x;
        if (y <= 0)
            return 1 / this.powerOf(base, x, ++y);
        return this.powerOf(base, x, --y);
    }

    public String divRepresentation(int x, int y) {
        return x + " = " + x / y + "*" + y + " + " + x % y;
    }

    public String primeFactorization(int x) {
        return x + " = " + primeFactor(x, 2);
    }

    private String primeFactor(int x, int factor) {
        if (x == 1)
            return "";
        if (factor > x)
            return x + "";
        if (factor == 2 && x % 2 == 0)
            return 2 + (x / 2 == 1 ? "" : "*" + primeFactor(x / 2, 2));
        else if (x % factor == 0 && isPrime(factor))
            return factor + (x / factor == 1 ? "" : "*" + primeFactor(x / factor, ++factor));
        return primeFactor(x, ++factor);
    }

    private boolean isPrime(int n) {
        if (n == (2) || n == (3)) {
            return true;
        } else if (n <= 1 || (n % 2) == (0) || (n % 3) == (0)) {
            return false;
        }
        int i = 5;
        while (pow(i, 2) <= n) {
            if ((n % i) == (0) || (n % (i + 2)) == (0)) {
                return false;
            }
            i += 6;
        }
        return true;
    }

}
