package com.company.calculator;

public class Customer {

    public static void main(String[] args) {

        Accountant accountant = new Accountant(1);
        System.out.println();
        accountant.calculate(-100, 22, Accountant.Operation.POWER);
        accountant.calculate(10, 15, Accountant.Operation.PRIME_FACTORS);
        accountant.calculate(10, 0, Accountant.Operation.DIVISION_REP);
        accountant.payout();
        accountant.calculate(10, 2, Accountant.Operation.ADD);
        accountant.calculate(10, 2, Accountant.Operation.DIVIDE);
        accountant.calculate(10, 2, Accountant.Operation.SUBTRACT);
        accountant.calculate(10, 2, Accountant.Operation.MULTIPLY);
        accountant.payout();

        Accountant accountant1 = new Accountant(2);
        accountant1.calculate(2, 100, Accountant.Operation.POWER);
        accountant1.calculate(-2, 4, Accountant.Operation.POWER);
        accountant1.calculate(-2, -2, Accountant.Operation.POWER);
        accountant1.calculate(2, -20, Accountant.Operation.POWER);
        accountant1.calculate(13, 2, Accountant.Operation.DIVISION_REP);
        accountant1.calculate(144, 0, Accountant.Operation.PRIME_FACTORS);
        accountant1.calculate(-10, 0, Accountant.Operation.PRIME_FACTORS);
        accountant1.payout();

        Accountant.printTotalOperations();

        System.out.println();
    }

}
