package com.company.calculator;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;

public class Accountant {
    private static int operationsPerformed;

    private static final Calculator calculator = new Calculator();
    private double totalCost;
    private final int operationCost;

    public Accountant(int operationCost) {
        this.operationCost = operationCost;
        this.totalCost = 0;
    }

    public void calculate(int x, int y, Operation operation) {
        if (operation.test(x, y)) {
            this.totalCost += this.operationCost;
            Accountant.operationsPerformed++;
            System.out.println(operation.execute(x, y));
        } else {
            System.out.println("Wrong Inputs " + operation.name() + " x = " + x + " y = " + y);
            this.totalCost += 0.5;
        }
    }

    public void payout() {
        System.out.println();
        System.out.println("Total Cost: " + this.totalCost);
        System.out.println();
        this.totalCost = 0;
    }

    public static void printTotalOperations() {
        System.out.println();
        System.out.println("Total Operations: " + Accountant.operationsPerformed);
        System.out.println();
    }

    //TODO - hide Predicate .test()
    public enum Operation implements BiPredicate<Integer, Integer> {
        ADD((x, y) -> x + "+" + y + " = " + calculator.add(x, y)),
        SUBTRACT((x, y) -> x + "-" + y + " = " + calculator.subtract(x, y)),
        DIVIDE((x, y) -> x + "/" + y + " = " + calculator.divide(x, y)),
        MULTIPLY((x, y) -> x + "*" + y + " = " + calculator.multiply(x, y)),
        DIVISION_REP(calculator::divRepresentation),
        POWER((x, y) -> x + "^" + y + " = " + calculator.pow(x, y)) {
            @Override
            public boolean test(Integer x, Integer y) {
                return true;
            }
        },
        PRIME_FACTORS((x, y) -> calculator.primeFactorization(x)) {
            @Override
            public boolean test(Integer x, Integer y) {
                return y == 0 && x > 1;
            }
        };

        private final BiFunction<Integer, Integer, String> operation;

        Operation(BiFunction<Integer, Integer, String> operation) {
            this.operation = operation;
        }

        private String execute(int x, int y) {
            return this.operation.apply(x, y);
        }

        @Override
        public boolean test(Integer x, Integer y) {
            return y <= 100 && y >= 1 && x <= 100 && x >= 1;
        }

    }

}
